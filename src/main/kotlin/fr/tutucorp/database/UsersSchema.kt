package fr.tutucorp.database

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

@Serializable
data class CreateUser(
    val name: String,
    val email: String,
    val bearer: String,
    val experience: Int
)

@Serializable
data class User(
    val id: Int,
    val name: String,
    val email: String,
    val experience: Int,
    val bearer: String
)

class UserService(database: Database) {
    object Users : Table() {
        val id = integer("id").autoIncrement()
        val name = varchar("name", length = 50)
        val email = varchar("email", length = 70)
        val bearer = varchar("bearer", length = 70)
        val experience = integer("experience")

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(Users)
        }
    }

    suspend fun create(user: CreateUser): Int = dbQuery {
        Users.insert {
            it[name] = user.name
            it[email] = user.email
            it[bearer] = user.bearer
            it[experience] = user.experience
        }[Users.id]
    }

    suspend fun readAll(): List<User> = dbQuery {
        Users.selectAll()
            .map {
                User(
                    id = it[Users.id],
                    name = it[Users.name],
                    email = it[Users.email],
                    bearer = it[Users.bearer],
                    experience = it[Users.experience],
                )
            }
    }

    suspend fun read(id: Int): User? = dbQuery {
        Users.select { Users.id eq id }
            .map {
                User(
                    id = it[Users.id],
                    name = it[Users.name],
                    email = it[Users.email],
                    bearer = it[Users.bearer],
                    experience = it[Users.experience],
                )
            }
            .singleOrNull()
    }

    suspend fun read(bearer: String): User? {
        return dbQuery {
            Users.select { Users.bearer eq bearer }
                .map {
                    User(
                        id = it[Users.id],
                        name = it[Users.name],
                        email = it[Users.email],
                        experience = it[Users.experience],
                        bearer = it[Users.bearer]
                    )
                }
                .singleOrNull()
        }
    }

    suspend fun userIdFromBearer(bearer: String): Int? {
        return dbQuery {
            Users.select { Users.bearer eq bearer }
                .map { it[Users.id] }
                .singleOrNull()
        }
    }

    suspend fun bearerFromEmail(email: String): String? {
        return dbQuery {
            Users.select { Users.email eq email }
                .map { it[Users.bearer] }
                .singleOrNull()
        }
    }

    suspend fun update(id: Int, user: User) {
        dbQuery {
            Users.update({ Users.id eq id }) {
                it[name] = user.name
                it[email] = user.email
                it[bearer] = user.bearer
                it[experience] = user.experience
            }
        }
    }

    suspend fun delete(token: String) {
        dbQuery {
            Users.deleteWhere { Users.bearer.eq(token) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            Users.deleteAll()
        }
    }
}
