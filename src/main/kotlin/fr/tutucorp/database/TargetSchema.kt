package fr.tutucorp.database

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreateTarget(
    val name: String
)

@Serializable
data class Target(
    val id: Int,
    val name: String
)

class TargetService(database: Database) {
    object Targets : Table() {
        val id = integer("id").autoIncrement()
        val name = varchar("name", length = 50).uniqueIndex()

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(Targets)
        }
    }

    suspend fun create(target: CreateTarget): Int = dbQuery {
        Targets.insert {
            it[name] = target.name
        }[Targets.id]
    }

    suspend fun readAll(): List<Target> {
        return dbQuery {
            Targets.selectAll()
                .map {
                    Target(
                        id = it[Targets.id],
                        name = it[Targets.name],
                    )
                }
        }
    }

    suspend fun read(id: Int): Target? {
        return dbQuery {
            Targets.select { Targets.id eq id }
                .map {
                    Target(
                        id = it[Targets.id],
                        name = it[Targets.name],
                    )
                }
                .singleOrNull()
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            Targets.deleteWhere { Targets.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            Targets.deleteAll()
        }
    }
}
