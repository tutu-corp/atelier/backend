package fr.tutucorp.database

import fr.tutucorp.database.PatternService.Patterns
import fr.tutucorp.database.SizeService.Sizes
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreatePatternSizes(val patternId: Int, val sizeId: Int)

@Serializable
data class PatternSize(val id: Int, val patternId: Int, val sizeId: Int)

class PatternSizeService(database: Database) {
    object PatternSizes : Table() {
        val id = integer("id").autoIncrement()
        val patternId = (integer("patternId") references Patterns.id)
        val sizeId = (integer("sizeId") references Sizes.id)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(PatternSizes)
        }
    }

    suspend fun create(pattern: CreatePatternSizes): Int = dbQuery {
        PatternSizes.insert {
            it[patternId] = pattern.patternId
            it[sizeId] = pattern.sizeId
        }[PatternSizes.id]
    }

    suspend fun readAll(): List<PatternSize> = dbQuery {
        PatternSizes.selectAll().map {
            PatternSize(
                id = it[PatternSizes.id],
                patternId = it[PatternSizes.patternId],
                sizeId = it[PatternSizes.sizeId]
            )
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            PatternSizes.deleteWhere { PatternSizes.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            PatternSizes.deleteAll()
        }
    }
}
