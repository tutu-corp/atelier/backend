package fr.tutucorp.database

import fr.tutucorp.database.PatternService.Patterns
import fr.tutucorp.database.TargetService.Targets
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreatePatternTarget(
    val patternId: Int,
    val targetId: Int
)

@Serializable
data class PatternTarget(
    val patternId: Int,
    val targetId: Int
)

class PatternTargetService(database: Database) {
    object PatternTargets : Table() {
        val id = integer("id").autoIncrement()
        val patternId = (integer("patternId") references Patterns.id)
        val targetId = (integer("targetId") references Targets.id)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(PatternTargets)
        }
    }

    suspend fun create(patternTarget: CreatePatternTarget): Int = dbQuery {
        PatternTargets.insert {
            it[patternId] = patternTarget.patternId
            it[targetId] = patternTarget.targetId
        }[PatternTargets.id]
    }

    suspend fun readAll(): List<PatternTarget> {
        return dbQuery {
            PatternTargets.selectAll()
                .map {
                    PatternTarget(
                        patternId = it[PatternTargets.patternId],
                        targetId = it[PatternTargets.targetId],
                    )
                }
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            PatternTargets.deleteWhere { PatternTargets.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            PatternTargets.deleteAll()
        }
    }
}
