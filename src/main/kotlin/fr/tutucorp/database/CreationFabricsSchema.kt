package fr.tutucorp.database

import fr.tutucorp.database.CreationService.Creations
import fr.tutucorp.database.FabricService.Fabrics
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreateCreationFabrics(val creationId: Int, val fabricId: Int)

@Serializable
data class CreationFabric(val id: Int, val creationId: Int, val fabricId: Int)

class CreationFabricsService(database: Database) {
    object CreationFabrics : Table() {
        val id = integer("id").autoIncrement()
        val creationId = (integer("creationId") references Creations.id)
        val fabricId = (integer("fabricId") references Fabrics.id)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(CreationFabrics)
        }
    }

    suspend fun create(creation: CreateCreationFabrics): Int = dbQuery {
        CreationFabrics.insert {
            it[creationId] = creation.creationId
            it[fabricId] = creation.fabricId
        }[CreationFabrics.id]
    }

    suspend fun readAll(): List<CreationFabric> = dbQuery {
        CreationFabrics.selectAll().map {
            CreationFabric(
                id = it[CreationFabrics.id],
                creationId = it[CreationFabrics.creationId],
                fabricId = it[CreationFabrics.fabricId]
            )
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            CreationFabrics.deleteWhere { CreationFabrics.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            CreationFabrics.deleteAll()
        }
    }
}
