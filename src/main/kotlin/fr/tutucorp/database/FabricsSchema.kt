package fr.tutucorp.database

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreateFabrics(val name: String)

@Serializable
data class Fabric(val id: Int, val name: String)

class FabricService(database: Database) {
    object Fabrics : Table() {
        val id = integer("id").autoIncrement()
        val name = varchar("name", length = 70)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(Fabrics)
        }
    }

    suspend fun create(pattern: CreateFabrics): Int = dbQuery {
        Fabrics.insert {
            it[name] = pattern.name
        }[Fabrics.id]
    }

    suspend fun readAll(): List<Fabric> {
        return dbQuery {
            Fabrics.selectAll()
                .map { Fabric(id = it[Fabrics.id], name = it[Fabrics.name]) }
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            Fabrics.deleteWhere { Fabrics.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            Fabrics.deleteAll()
        }
    }
}
