package fr.tutucorp.database

import fr.tutucorp.CREATION_IMAGES_PATH
import fr.tutucorp.SERVER_NAME
import fr.tutucorp.database.CreationFabricsService.CreationFabrics
import fr.tutucorp.database.CreationImagesService.CreationImages
import fr.tutucorp.database.FabricService.Fabrics
import fr.tutucorp.database.PatternService.Patterns
import fr.tutucorp.database.SizeService.Sizes
import fr.tutucorp.database.UserService.Users
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.FieldSet
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreateCreation(val patternId: Int, val userId: Int, val sizeId: Int)

@Serializable
data class Creation(
    val id: Int,
    val patternName: String,
    val creationDate: LocalDateTime,
    val user: User,
    val size: Size,
    val images: List<String>,
    val fabrics: List<String>
) {

    @Serializable
    data class Size(val code: String, val type: String)

    @Serializable
    data class User(val id: Int, val name: String, val experience: Int)
}

class CreationService(database: Database) {
    object Creations : Table() {
        val id = integer("id").autoIncrement()
        val patternId = (integer("patternId") references Patterns.id)
        val userId = (integer("userId") references Users.id)
        val sizeId = (integer("sizeId") references Sizes.id)
        val date = (text("date"))

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(Creations)
        }
    }

    suspend fun create(creation: CreateCreation): Int = dbQuery {
        Creations.insert {
            it[patternId] = creation.patternId
            it[userId] = creation.userId
            it[sizeId] = creation.sizeId
            it[date] = Clock.System.now().toString()
        }[Creations.id]
    }

    suspend fun readAll(): List<Creation> = dbQuery {
        getCreation()
            .selectAll()
            .map { it.toCreation() }
    }

    suspend fun read(id: Int): Creation? = dbQuery {
        getCreation()
            .select { Creations.id eq id }
            .map { it.toCreation() }
            .singleOrNull()
    }

    suspend fun readForPattern(id: Int): List<Creation> = dbQuery {
        getCreation()
            .select { Creations.patternId eq id }
            .map { it.toCreation() }
    }

    suspend fun readForUser(id: Int): List<Creation> = dbQuery {
        getCreation()
            .select { Creations.userId eq id }
            .map { it.toCreation() }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            Creations.deleteWhere { Creations.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            Creations.deleteAll()
        }
    }

    private fun getCreation(): FieldSet = Creations.join(Patterns, JoinType.INNER) {
        Creations.patternId eq Patterns.id
    }
        .join(Users, JoinType.INNER) {
            Creations.userId eq Users.id
        }
        .join(Sizes, JoinType.INNER) {
            Creations.sizeId eq Sizes.id
        }
        .slice(Creations.id, Patterns.name, Users.id, Users.name, Sizes.code, Sizes.type, Creations.date)

    private fun ResultRow.toCreation(): Creation {
        val it = this
        return Creation(
            id = it[Creations.id],
            patternName = it[Patterns.name],
            user = Creation.User(
                id = it[Users.id],
                name = it[Users.name],
                experience = it[Users.experience]
            ),
            size = Creation.Size(
                code = it[Sizes.code],
                type = it[Sizes.type]
            ),
            fabrics = CreationFabrics
                .join(Fabrics, JoinType.INNER)
                .slice(Fabrics.name)
                .select {
                    CreationFabrics.creationId eq it[Creations.id]
                }.map { cursor -> cursor[Fabrics.name] },
            images = CreationImages
                .select {
                    CreationImages.creationId eq it[Creations.id]
                }.map { cursor ->
                    "$SERVER_NAME$CREATION_IMAGES_PATH/${cursor[CreationImages.filePath]}"
                },
            creationDate = it[Creations.date].toInstant().toLocalDateTime(TimeZone.currentSystemDefault())
        )
    }
}
