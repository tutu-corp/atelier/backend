package fr.tutucorp.database

import fr.tutucorp.CREATION_IMAGES_PATH
import fr.tutucorp.SERVER_NAME
import fr.tutucorp.database.CreationService.Creations
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreateCreationImages(val creationId: Int, val filePath: String)

@Serializable
data class CreationImage(val id: Int, val creationId: Int, val filePath: String)

class CreationImagesService(database: Database) {
    object CreationImages : Table() {
        val id = integer("id").autoIncrement()
        val creationId = (integer("creationId") references Creations.id)
        val filePath = varchar("filePath", 100)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(CreationImages)
        }
    }

    suspend fun create(creation: CreateCreationImages): Int = dbQuery {
        CreationImages.insert {
            it[creationId] = creation.creationId
            it[filePath] = creation.filePath
        }[CreationImages.id]
    }

    suspend fun readAll(): List<CreationImage> = dbQuery {
        CreationImages.selectAll().map {
            CreationImage(
                id = it[CreationImages.id],
                creationId = it[CreationImages.creationId],
                filePath = "$SERVER_NAME$CREATION_IMAGES_PATH/${it[CreationImages.filePath]}"
            )
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            CreationImages.deleteWhere { CreationImages.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            CreationImages.deleteAll()
        }
    }
}
