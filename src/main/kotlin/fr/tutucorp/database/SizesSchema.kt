package fr.tutucorp.database

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreateSize(val code: String, val type: String)

@Serializable
data class Size(val id: Int, val code: String, val type: String)

class SizeService(database: Database) {
    object Sizes : Table() {
        val id = integer("id").autoIncrement()
        val code = varchar("code", length = 70).uniqueIndex()
        val type = varchar("type", length = 70)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(Sizes)
        }
    }

    suspend fun create(size: CreateSize): Int = dbQuery {
        Sizes.insert {
            it[code] = size.code
            it[type] = size.type
        }[Sizes.id]
    }

    suspend fun readAll(): List<Size> {
        return dbQuery {
            Sizes.selectAll()
                .map {
                    Size(
                        id = it[Sizes.id],
                        code = it[Sizes.code],
                        type = it[Sizes.type]
                    )
                }
        }
    }

    suspend fun read(id: Int): Size? {
        return dbQuery {
            Sizes.select { Sizes.id eq id }
                .map {
                    Size(
                        id = it[Sizes.id],
                        code = it[Sizes.code],
                        type = it[Sizes.type]
                    )
                }
                .singleOrNull()
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            Sizes.deleteWhere { Sizes.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            Sizes.deleteAll()
        }
    }
}
