package fr.tutucorp.database

import fr.tutucorp.PATTERNS_IMAGES_PATH
import fr.tutucorp.SERVER_NAME
import fr.tutucorp.database.PatternSizeService.PatternSizes
import fr.tutucorp.database.PatternTargetService.PatternTargets
import fr.tutucorp.database.SizeService.Sizes
import fr.tutucorp.database.TargetService.Targets
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
data class CreatePattern(
    val name: String,
    val creatorName: String,
    val filePath: String
)

@Serializable
data class Pattern(
    val id: Int,
    val name: String,
    val creatorName: String,
    val filePath: String,
    val sizes: List<String>,
    val targets: List<String>
)

class PatternService(database: Database) {
    object Patterns : Table() {
        val id = integer("id").autoIncrement()
        val name = varchar("name", length = 50).uniqueIndex()
        val creatorName = text("creatorName")
        val filePath = varchar("filePath", length = 100)

        override val primaryKey = PrimaryKey(id)
    }

    init {
        transaction(database) {
            SchemaUtils.create(Patterns)
        }
    }

    suspend fun create(pattern: CreatePattern): Int = dbQuery {
        Patterns.insert {
            it[name] = pattern.name
            it[filePath] = pattern.filePath
            it[creatorName] = pattern.creatorName
        }[Patterns.id]
    }

    suspend fun readAll(): List<Pattern> {
        return dbQuery {
            Patterns.selectAll()
                .map {
                    Pattern(
                        id = it[Patterns.id],
                        name = it[Patterns.name],
                        creatorName = it[Patterns.creatorName],
                        filePath = "$SERVER_NAME$PATTERNS_IMAGES_PATH/${it[Patterns.filePath]}",
                        sizes = PatternSizes
                            .join(Sizes, JoinType.INNER)
                            .slice(Sizes.code)
                            .select { PatternSizes.patternId eq it[Patterns.id] }
                            .map { sizes ->
                                sizes[Sizes.code]
                            },
                        targets = PatternTargets
                            .join(Targets, JoinType.INNER)
                            .slice(Targets.name)
                            .select { PatternTargets.patternId eq it[Patterns.id] }
                            .map { targets ->
                                targets[Targets.name]
                            }
                    )
                }
        }
    }

    suspend fun read(id: Int): Pattern? {
        return dbQuery {
            Patterns.select { Patterns.id eq id }
                .map {
                    Pattern(
                        id = it[Patterns.id],
                        name = it[Patterns.name],
                        creatorName = it[Patterns.creatorName],
                        filePath = "$SERVER_NAME$PATTERNS_IMAGES_PATH/${it[Patterns.filePath]}",
                        sizes = PatternSizes
                            .join(Sizes, JoinType.INNER)
                            .slice(Sizes.code)
                            .select { PatternSizes.patternId eq it[Patterns.id] }
                            .map { sizes ->
                                sizes[Sizes.code]
                            },
                        targets = PatternTargets
                            .join(Targets, JoinType.INNER)
                            .slice(Targets.name)
                            .select { PatternTargets.patternId eq it[Patterns.id] }
                            .map { targets ->
                                targets[Targets.name]
                            }
                    )
                }
                .singleOrNull()
        }
    }

    suspend fun delete(id: Int) {
        dbQuery {
            Patterns.deleteWhere { Patterns.id.eq(id) }
        }
    }

    suspend fun deleteAll() {
        dbQuery {
            Patterns.deleteAll()
        }
    }
}
