package fr.tutucorp

import fr.tutucorp.database.CreationFabricsService
import fr.tutucorp.database.CreationImagesService
import fr.tutucorp.database.CreationService
import fr.tutucorp.database.FabricService
import fr.tutucorp.database.PatternService
import fr.tutucorp.database.PatternSizeService
import fr.tutucorp.database.PatternTargetService
import fr.tutucorp.database.SizeService
import fr.tutucorp.database.TargetService
import fr.tutucorp.database.UserService
import fr.tutucorp.plugins.configureSecurity
import fr.tutucorp.plugins.configureSerialization
import fr.tutucorp.routing.backend.creationFabricsRoutes
import fr.tutucorp.routing.backend.creationImagesRoutes
import fr.tutucorp.routing.backend.creationRoutes
import fr.tutucorp.routing.backend.fabricRoutes
import fr.tutucorp.routing.backend.patternRoutes
import fr.tutucorp.routing.backend.patternSizesRoutes
import fr.tutucorp.routing.backend.patternTargetRoutes
import fr.tutucorp.routing.backend.sizeRoutes
import fr.tutucorp.routing.backend.targetRoutes
import fr.tutucorp.routing.backend.userBackendRoutes
import fr.tutucorp.routing.homeRoutes
import fr.tutucorp.routing.userRoutes
import io.ktor.server.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.http.content.staticFiles
import io.ktor.server.netty.Netty
import io.ktor.server.routing.routing
import java.io.File
import org.jetbrains.exposed.sql.Database

const val PATTERNS_IMAGES_PATH = "images/patterns"
const val CREATION_IMAGES_PATH = "images/creations"
const val SERVER_NAME = "https://127.0.0.1:8080/" // I KNOW Leave me alone !

val dir = File("build/db")

val database = Database.connect(
    url = "jdbc:h2:file:${dir.canonicalFile.absolutePath}",
    user = "root",
    driver = "org.h2.Driver",
    password = ""
)
val userService = UserService(database)
val patternService = PatternService(database)
val creationService = CreationService(database)
val fabricService = FabricService(database)
val sizeService = SizeService(database)
val targetService = TargetService(database)
val creationFabricsService = CreationFabricsService(database)
val creationImagesService = CreationImagesService(database)
val patternSizeService = PatternSizeService(database)
val patternTargetService = PatternTargetService(database)

fun main() {
    embeddedServer(Netty, port = 8080, host = "127.0.0.1", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    // configuration
    configureSecurity(userService = userService)
    configureSerialization()
    // routes
    homeRoutes(creationService = creationService, patternService = patternService)
    userRoutes(userService = userService, creationService = creationService)
    // backend routes
    userBackendRoutes(userService = userService)
    patternRoutes(patternService = patternService)
    creationRoutes(creationService = creationService)
    fabricRoutes(fabricService = fabricService)
    sizeRoutes(sizeService = sizeService)
    targetRoutes(targetService = targetService)
    creationFabricsRoutes(creationFabricsService = creationFabricsService)
    creationImagesRoutes(creationImagesService = creationImagesService)
    patternSizesRoutes(patternSizeService = patternSizeService)
    patternTargetRoutes(patternTargetService = patternTargetService)
    // static files
    routing {
        staticFiles(remotePath = PATTERNS_IMAGES_PATH, dir = File(PATTERNS_IMAGES_PATH))
        staticFiles(remotePath = CREATION_IMAGES_PATH, dir = File(CREATION_IMAGES_PATH))
    }
}
