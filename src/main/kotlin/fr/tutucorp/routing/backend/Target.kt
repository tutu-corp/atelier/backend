package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreateTarget
import fr.tutucorp.database.TargetService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.targetRoutes(targetService: TargetService) {
    routing {
        get("/backend/targets") {
            val sizes = targetService.readAll()
            call.respond(HttpStatusCode.OK, sizes)
        }
        post("/backend/targets") {
            val target = call.receive<CreateTarget>()
            call.respond(HttpStatusCode.Created, targetService.create(target))
        }
        delete("/backend/targets") {
            targetService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/targets/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            targetService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/targets") {
            targetService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
