package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreateCreation
import fr.tutucorp.database.CreationService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.creationRoutes(creationService: CreationService) {
    routing {
        get("/backend/creations") {
            try {
                val creations = creationService.readAll()
                call.respond(HttpStatusCode.OK, creations)
            } catch (e: Exception) {
                call.respond(HttpStatusCode.InternalServerError, e.stackTraceToString())
            }
        }
        post("/backend/creations") {
            try {
                val creation = call.receive<CreateCreation>()
                val id = creationService.create(creation)
                call.respond(HttpStatusCode.Created, id)
            } catch (e: Exception) {
                call.respond(HttpStatusCode.InternalServerError, e.stackTraceToString())
            }
        }
        delete("/backend/creations") {
            creationService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/creations/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            creationService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/creations") {
            creationService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
