package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreateFabrics
import fr.tutucorp.database.FabricService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.fabricRoutes(fabricService: FabricService) {
    routing {
        get("/backend/fabrics") {
            val fabrics = fabricService.readAll()
            call.respond(HttpStatusCode.OK, fabrics)
        }
        post("/backend/fabrics") {
            val fabric = call.receive<CreateFabrics>()
            val id = fabricService.create(fabric)
            call.respond(HttpStatusCode.Created, id)
        }
        delete("/backend/fabrics") {
            fabricService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/fabrics/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            fabricService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/fabrics") {
            fabricService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
