package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreateUser
import fr.tutucorp.database.UserService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.userBackendRoutes(userService: UserService) {
    routing {
        get("/backend/users") {
            val user = userService.readAll()
            call.respond(HttpStatusCode.OK, user)
        }
        get("/backend/users/{bearer}") {
            val bearer = call.parameters["bearer"] ?: throw IllegalArgumentException("Invalid ID")
            val user = userService.read(bearer = bearer)
            if (user != null) {
                call.respond(HttpStatusCode.OK, user)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        post("/backend/users") {
            val user = call.receive<CreateUser>()
            val id = userService.create(user)
            call.respond(HttpStatusCode.Created, id)
        }
        delete("/backend/users/{token}") {
            val token = call.parameters["token"] ?: throw IllegalArgumentException("Invalid token")
            userService.delete(token = token)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/users") {
            userService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
