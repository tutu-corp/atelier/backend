package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreateSize
import fr.tutucorp.database.SizeService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.sizeRoutes(sizeService: SizeService) {
    routing {
        get("/backend/sizes") {
            val sizes = sizeService.readAll()
            call.respond(HttpStatusCode.OK, sizes)
        }
        post("/backend/sizes") {
            val sizes = call.receive<List<CreateSize>>()
            call.respond(
                HttpStatusCode.Created,
                sizes.map { sizeService.create(it) }
            )
        }
        delete("/backend/sizes") {
            sizeService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/sizes/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            sizeService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/sizes") {
            sizeService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
