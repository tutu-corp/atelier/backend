package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreatePatternSizes
import fr.tutucorp.database.PatternSizeService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.patternSizesRoutes(patternSizeService: PatternSizeService) {
    routing {
        get("/backend/pattern_sizes") {
            val patternSizes = patternSizeService.readAll()
            call.respond(HttpStatusCode.OK, patternSizes)
        }
        post("/backend/pattern_sizes") {
            try {
                val patternSize = call.receive<CreatePatternSizes>()
                val id = patternSizeService.create(patternSize)
                call.respond(HttpStatusCode.Created, id)
            } catch (e: Exception) {
                call.respond(HttpStatusCode.InternalServerError, e.stackTraceToString())
            }
        }
        delete("/backend/pattern_sizes") {
            patternSizeService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/pattern_sizes/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            patternSizeService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/pattern_sizes") {
            patternSizeService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
