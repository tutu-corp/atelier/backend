package fr.tutucorp.routing.backend

import fr.tutucorp.PATTERNS_IMAGES_PATH
import fr.tutucorp.database.CreatePattern
import fr.tutucorp.database.PatternService
import fr.tutucorp.utils.save
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.forEachPart
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receiveMultipart
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing
import java.io.File

fun Application.patternRoutes(patternService: PatternService) {
    routing {
        get("/backend/patterns") {
            val patterns = patternService.readAll()
            call.respond(HttpStatusCode.OK, patterns)
        }
        post("/backend/patterns") {
            val multipart = call.receiveMultipart()
            var fileName: String? = null
            var name: String? = null
            var creatorName: String? = null
            try {
                multipart.forEachPart { partData ->
                    when (partData) {
                        is PartData.FormItem -> {
                            when (partData.name) {
                                "name" -> name = partData.value
                                "creatorName" -> creatorName = partData.value
                            }
                        }

                        is PartData.FileItem -> {
                            fileName = partData.save(PATTERNS_IMAGES_PATH)
                        }

                        is PartData.BinaryItem -> Unit
                        is PartData.BinaryChannelItem -> Unit
                    }
                }

                val pattern = CreatePattern(name = name!!, creatorName = creatorName!!, filePath = fileName!!)
                val id = patternService.create(pattern)
                call.respond(HttpStatusCode.Created, id)
            } catch (ex: Exception) {
                ex.printStackTrace()
                File("${PATTERNS_IMAGES_PATH}/$fileName").delete()
                call.respond(HttpStatusCode.InternalServerError, "Error")
            }
        }
        delete("/backend/patterns") {
            patternService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/patterns/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            patternService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/patterns") {
            patternService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
