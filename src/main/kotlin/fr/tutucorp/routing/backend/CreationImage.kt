package fr.tutucorp.routing.backend

import fr.tutucorp.CREATION_IMAGES_PATH
import fr.tutucorp.database.CreateCreationImages
import fr.tutucorp.database.CreationImagesService
import fr.tutucorp.utils.save
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.forEachPart
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receiveMultipart
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing
import java.io.File

fun Application.creationImagesRoutes(creationImagesService: CreationImagesService) {
    routing {
        get("/backend/creation_images") {
            val creationFabrics = creationImagesService.readAll()
            call.respond(HttpStatusCode.OK, creationFabrics)
        }
        post("/backend/creation_images") {
            val multipart = call.receiveMultipart()
            var id: Int? = null
            val fileNames: MutableList<String> = mutableListOf()
            try {
                multipart.forEachPart { partData ->
                    when (partData) {
                        is PartData.FormItem -> if (partData.name == "id") {
                            id = partData.value.toInt()
                        }

                        is PartData.FileItem ->
                            fileNames += partData.save(CREATION_IMAGES_PATH)

                        is PartData.BinaryItem -> Unit
                        is PartData.BinaryChannelItem -> Unit
                    }
                }

                val ids = fileNames.map {
                    val images = CreateCreationImages(creationId = id!!, filePath = it)
                    creationImagesService.create(images)
                }
                call.respond(HttpStatusCode.Created, ids)
            } catch (ex: Exception) {
                ex.printStackTrace()
                fileNames.forEach {
                    File("$CREATION_IMAGES_PATH/$it").delete()
                }
                call.respond(HttpStatusCode.InternalServerError, "Error")
            }
        }
        delete("/backend/creation_images") {
            creationImagesService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/creation_images/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            creationImagesService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/creation_images") {
            creationImagesService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
