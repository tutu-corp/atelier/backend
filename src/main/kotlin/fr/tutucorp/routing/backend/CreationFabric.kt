package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreateCreationFabrics
import fr.tutucorp.database.CreationFabricsService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.creationFabricsRoutes(creationFabricsService: CreationFabricsService) {
    routing {
        get("/backend/creation_fabrics") {
            val creationFabrics = creationFabricsService.readAll()
            call.respond(HttpStatusCode.OK, creationFabrics)
        }
        post("/backend/creation_fabrics") {
            val creationFabric = call.receive<CreateCreationFabrics>()
            val id = creationFabricsService.create(creationFabric)
            call.respond(HttpStatusCode.Created, id)
        }
        delete("/backend/creation_fabrics") {
            creationFabricsService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/creation_fabrics/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            creationFabricsService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/creation_fabrics") {
            creationFabricsService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
