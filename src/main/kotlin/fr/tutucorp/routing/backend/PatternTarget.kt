package fr.tutucorp.routing.backend

import fr.tutucorp.database.CreatePatternTarget
import fr.tutucorp.database.PatternTargetService
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.delete
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.patternTargetRoutes(patternTargetService: PatternTargetService) {
    routing {
        get("/backend/pattern_targets") {
            val sizes = patternTargetService.readAll()
            call.respond(HttpStatusCode.OK, sizes)
        }
        post("/backend/pattern_targets") {
            val target = call.receive<CreatePatternTarget>()
            call.respond(HttpStatusCode.Created, patternTargetService.create(target))
        }
        delete("/backend/pattern_targets") {
            patternTargetService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/pattern_targets/{id}") {
            val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
            patternTargetService.delete(id = id)
            call.respond(HttpStatusCode.OK)
        }
        delete("/backend/pattern_targets") {
            patternTargetService.deleteAll()
            call.respond(HttpStatusCode.OK)
        }
    }
}
