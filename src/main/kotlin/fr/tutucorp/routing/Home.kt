package fr.tutucorp.routing

import fr.tutucorp.database.CreationService
import fr.tutucorp.database.PatternService
import fr.tutucorp.models.Home
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.auth.authenticate
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.routing

fun Application.homeRoutes(
    creationService: CreationService,
    patternService: PatternService
) {
    routing {
        authenticate {
            get("/") {
                val creations = creationService.readAll()
                val home = Home(creations = creations)
                call.respond(HttpStatusCode.OK, home)
            }
            get("/creations/{id}") {
                val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
                val creation = creationService.read(id)
                if (creation == null) {
                    call.respond(HttpStatusCode.NotFound)
                } else {
                    call.respond(HttpStatusCode.OK, creation)
                }
            }
            get("/patterns") {
                val patterns = patternService.readAll()
                if (patterns.isNotEmpty()) {
                    call.respond(HttpStatusCode.OK, patterns)
                } else {
                    call.respond(HttpStatusCode.NotFound)
                }
            }
            get("/patterns/{id}") {
                val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
                val pattern = patternService.read(id)
                if (pattern != null) {
                    call.respond(HttpStatusCode.OK, pattern)
                } else {
                    call.respond(HttpStatusCode.NotFound)
                }
            }
            get("/patterns/{id}/creations") {
                val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
                val creations = creationService.readForPattern(id)
                if (creations.isEmpty()) {
                    call.respond(HttpStatusCode.NotFound)
                } else {
                    call.respond(HttpStatusCode.OK, creations)
                }
            }
        }
    }
}
