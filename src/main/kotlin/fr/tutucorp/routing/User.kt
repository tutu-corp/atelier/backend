package fr.tutucorp.routing

import fr.tutucorp.database.CreationService
import fr.tutucorp.database.UserService
import fr.tutucorp.models.Login
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.auth.UserIdPrincipal
import io.ktor.server.auth.authenticate
import io.ktor.server.auth.principal
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.post
import io.ktor.server.routing.routing

fun Application.userRoutes(
    userService: UserService,
    creationService: CreationService
) {
    routing {
        post("/login") {
            val login = call.receive<Login>()
            val bearer = userService.bearerFromEmail(email = login.email)
            if (bearer != null) {
                call.respond(HttpStatusCode.OK, bearer)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        authenticate {
            get("/users/{id}") {
                val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
                val user = userService.read(id)
                if (user == null) {
                    call.respond(HttpStatusCode.NotFound)
                } else {
                    call.respond(HttpStatusCode.OK, user)
                }
            }
            get("/users/me") {
                val id = call.principal<UserIdPrincipal>()?.name ?: throw IllegalArgumentException("Invalid id")
                val user = userService.read(id = id.toInt())
                if (user == null) {
                    call.respond(HttpStatusCode.NotFound)
                } else {
                    call.respond(HttpStatusCode.OK, user)
                }
            }
            get("/users/{id}/creations") {
                val id = call.parameters["id"]?.toInt() ?: throw IllegalArgumentException("Invalid id")
                val creations = creationService.readForUser(id)
                if (creations.isEmpty()) {
                    call.respond(HttpStatusCode.NotFound)
                } else {
                    call.respond(HttpStatusCode.OK, creations)
                }
            }
        }
    }
}
