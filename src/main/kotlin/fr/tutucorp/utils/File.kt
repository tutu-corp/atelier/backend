package fr.tutucorp.utils

import io.ktor.http.content.PartData
import io.ktor.http.content.streamProvider
import java.io.File
import java.util.UUID

fun PartData.FileItem.save(path: String): String {
    val fileBytes = streamProvider().readBytes()
    val fileExtension = originalFileName?.takeLastWhile { it != '.' }
    val fileName = UUID.randomUUID().toString() + "." + fileExtension
    println("Path = $path/$fileName")
    val folder = File(path)
    // create parent directory if not exits
    if (!folder.parentFile.exists()) {
        folder.parentFile.mkdirs()
    }
    folder.mkdir()
    File("$path/$fileName").writeBytes(fileBytes)
    return fileName
}
