package fr.tutucorp.models

import kotlinx.serialization.Serializable

@Serializable
data class Login(val email: String)
