package fr.tutucorp.models

import fr.tutucorp.database.Creation
import kotlinx.serialization.Serializable

@Serializable
data class Home(
    val creations: List<Creation>
)
