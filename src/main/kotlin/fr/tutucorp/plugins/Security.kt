package fr.tutucorp.plugins

import fr.tutucorp.database.UserService
import io.ktor.server.application.Application
import io.ktor.server.auth.UserIdPrincipal
import io.ktor.server.auth.authentication
import io.ktor.server.auth.bearer

fun Application.configureSecurity(userService: UserService) {
    authentication {
        bearer {
            realm = "tutu"
            authenticate { tokenCredential ->
                userService.userIdFromBearer(bearer = tokenCredential.token)?.let {
                    UserIdPrincipal(it.toString())
                }
            }
        }
    }
}
